@extends('layout.layout_adminlte')

@section('judul')
LIST FILM  {{--INI BUAT JUDUL --}}
@endsection

@section('content')

<div class="row">
    @forelse ($film as $item)
<div class="col-4"> <!-- 12 dibagi 3 = 4-->
            
            @auth
            <a href="/film/create" class="btn btn-success mb-3">Add Film</a>
            @endauth
            
    <div class="card">
        <img height="200px" class="card-img-top" src="{{asset('poster/'.$item->poster)}}" alt="Card image cap">
        <div class="card-body">
            {{-- selipin disini one to many nya dari tabel cast --}}
            <span class="badge badge-info">{{$item->cast->nama}}</span>
            <h5>{{$item->judul}}</h5>
            <p class="card-text">{{ Str::limit($item->ringkasan,20)}}</p>
            <p>Tanggal di Upload : {{($item->created_at)}}</p>
            {{-- form delete --}}
            @auth
                
            <form action="/film/{{$item->id}}" method="POST">
                @csrf
                @method('Delete')
                <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/film/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                <input type="submit" onclick="return confirm('Are You Sure?')" class="btn btn-danger btn-sm" value="Delete">
            </form>
            @endauth

            @guest
                <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            @endguest
    
        </div>
  </div>
</div>
    @empty
        <h1>DATA FILM MASIH KOSONG !</h1>
    @endforelse

</div>
{{$film->links()}}



@endsection