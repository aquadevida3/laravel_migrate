@extends('layout.layout_adminlte')

@section('judul')
TAMBAH FILM  {{--INI BUAT JUDUL --}}
@endsection

@section('content')

<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul">
      
        @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
      <label>Ringkasan</label>
      <input type="text" class="form-control" name="ringkasan">
    
        @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label>
        <input type="text" class="form-control" name="tahun">
      
      @error('tahun')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label>Cast</label>
      <select name="cast_id" id="" class="form-control">
          <option value="">-----pilih cast-----</option>
          @foreach ($cast as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>    
          @endforeach
      </select>
    @error('cast_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    </div>
    
    <div class="form-group">
    <label>Poster</label>
    <input type="file" class="form-control" name="poster">
            
      @error('poster')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>

    <button type="submit" class="btn btn-primary">Save</button>

</form>

@endsection