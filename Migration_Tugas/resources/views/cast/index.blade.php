@extends('layout.layout_adminlte')

@section('judul')
HALAMAN INDEX    {{--INI BUAT JUDUL --}}
@endsection



@section('content')


<a href="/cast/create" class="btn btn-success mb-3">Add Cast</a>
        <table class="table" id="example1">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Biodata</th>
                <th scope="col">List Film</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value) <!--jika datanya ada isinya maka tampilkan yang ini-->
                    <tr>
                        <td>{{$key + 1}}</th>   {{-- supaya nomor di depan tampil 12345 --}}
                        <td>{{$value->nama}}</td> <!-- sesuaikan sama colom di database-->
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                                {{-- karna dia lebih dari satu data yang tampil kita pakai foreach aja --}}
                        <ul>

                            @foreach ($value->film as $item)
                                <li>{{$item->judul}}</li>
                                
                            @endforeach

                        </ul>
                        </td>
                        <td>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/cast/{{$value->id}}" class="btn btn-info">Detail</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <input type="submit" onclick="return confirm('Are You Sure?')" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty <!--jika datanya isinya kosong maka tampilkan yang ini-->
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        {{$cast->links()}}
@endsection


